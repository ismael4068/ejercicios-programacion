#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
    int numero = 0;
    int sumar = 0;

    printf("Programa que realiza una suma de numeros consecutivos");
    for (int i=0; i<50; i++)
    {
        printf("Introduce un numero del 1 al 50 : ");
        scanf("%d",&numero);
        while (numero>50)
        {
            printf("Introduce otro numero del 1 al 50 : ");
            scanf("%d",&numero);
        }
        sumar+=numero;
    }
    printf("La suma de los numeros es: %d",sumar);
    return 0;
}
